"""
tootrss
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from bs4 import BeautifulSoup
from feedparser import FeedParserDict


def make_toot_text(entry: FeedParserDict) -> str:
    """Concocts a toot that fits in 500 characters"""
    text = entry.title

    if summary := getattr(entry, "summary", ""):
        text = strip_html(f"{text}\n\n{strip_html(summary)}")
    # Cut text to leave enough space for link
    # Links always take 23 chars
    # Leave enough space for two newlines
    text = text[:(500 - 23 - 2)]

    return f"{text}\n\n{entry.link}"


def strip_html(text: str) -> str:
    """
    Strip HTML from toot text

    Some RSS feeds use HTML in their descriptions and maybe elsewhere.
    Mastodon doesn't interpret HTML and presents it as is, which is quite ugly.
    """
    soup = BeautifulSoup(text, "lxml")
    return " ".join(soup.stripped_strings).strip()
