"""
tootrss
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
from typing import Dict

import validators
from pydantic import BaseModel, Field, validator

VALID_KEY_RE = r"[_a-z0-9]{3,}"
VALID_DOMAIN = r"([_a-z0-9]+\.)*[_a-z0-9]{3,}\.[_a-z0-9]{2,}"
DEFAULT_CONFIG = {
    "feeds": {}
}


#######
# Config models
#######


class ClientCredentials(BaseModel):
    """Hold credential necessary to make a Mastodon client"""
    access_token: str
    client_id: str
    client_secret: str


class Feed(BaseModel):
    """Where to get the RSS feed and how to toot its entries"""
    url: str
    credentials: ClientCredentials

    @validator("url")
    def validate_url(cls, url):
        if not (url.startswith("https://") and validators.url(url)):
            raise ValueError("Feed must be a valid HTTPS URL")
        return url


class Config(BaseModel):
    server: str
    feeds: Dict[str, Feed] = Field(default_factory=dict)

    @validator("server")
    def validate_server(cls, server):
        if not re.match(VALID_DOMAIN, server):
            raise ValueError("Server must be a valid domain name")
        return server

    @validator("feeds")
    def validate_feeds(cls, feeds):
        for key, feed in feeds.items():
            if not re.match(VALID_KEY_RE, key):
                raise ValueError("Key must be a valid mastodon name", key)
        return feeds
