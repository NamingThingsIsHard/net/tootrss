"""
tooterss
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import copy
import logging
from pathlib import Path
from typing import List

import feedparser
import yaml
from feedparser import FeedParserDict
from mastodon import Mastodon
from pydantic import ValidationError

import tootrss
from tootrss.config import Config, DEFAULT_CONFIG
from tootrss.masto import make_toot_text


def main(yaml_config: dict, cache_dir: Path):
    _config = copy.deepcopy(DEFAULT_CONFIG)
    _config.update(yaml_config or {})
    try:
        config = Config.parse_obj(_config)
    except ValidationError as val_error:
        logging.error("Couldn't validate config: %s", val_error)
        return 1

    if not config.feeds:
        logging.warning("Cannot find any feeds in config. Exiting...")
        return 0

    # Create cache_dir if necessary
    cache_dir.mkdir(parents=True, exist_ok=True)

    server = config.server
    server_url = f"https://{server}"
    logging.info("Will use server: %s", server_url)

    client_cache = {}
    logging.info("Updating %s feeds", len(config.feeds))
    # Update the feeds
    for account_name, feed in config.feeds.items():
        credentials = feed.credentials
        mastodon = client_cache.get(account_name)
        if not mastodon:
            mastodon = Mastodon(
                client_id=credentials.client_id,
                client_secret=credentials.client_secret,
                access_token=credentials.access_token,
                api_base_url=server_url,
                debug_requests=False,
            )
            client_cache[account_name] = mastodon

        # Retrieve RSS feed and filter out the old
        # The feed entries are identified by their URL (which should be unique)
        cache_file = cache_dir / f"{account_name}.lst"
        if not cache_file.is_file():
            cache_file.write_text("")

        cache = set(cache_file.read_text().split("\n"))
        entries = feedparser.parse(feed.url).get('entries', [])
        new_entries: List[FeedParserDict] = [item for item in entries if item.link not in cache]

        if new_entries:
            logging.info("Found {} new items for feed {}".format(len(new_entries), feed.url))

        # Toot the new feed entries (those that aren't in the cache)
        with open(cache_file, "a") as f:
            toot_count = 0
            for entry in new_entries:
                try:
                    send_toot(entry, mastodon)
                    f.write(f"{entry.link}\n")
                    toot_count += 1
                except Exception:
                    logging.warning("Couldn't post status for entry %s", entry, exc_info=True)
        logging.info("Sent out %s toots", toot_count)


def send_toot(entry: FeedParserDict, mastodon: Mastodon):
    media_ids = []
    text = make_toot_text(entry)
    mastodon.status_post(text, media_ids=media_ids)


def YamlPath(arg: str) -> dict:
    if not Path(arg).is_file():
        raise argparse.ArgumentTypeError("Must be a file")
    try:
        with open(arg) as file:
            return yaml.safe_load(file)
    except Exception as e:
        raise argparse.ArgumentTypeError("Not a valid YAML file") from e


def DirPath(arg: str) -> Path:
    path = Path(arg)
    if not path.is_dir() and path.exists():
        raise argparse.ArgumentTypeError("Must be a directory or not exist")
    return path


parser = argparse.ArgumentParser("tootrss")
parser.add_argument('--version', action='version', version=f'%(prog)s {tootrss.__version__}')
parser.add_argument("--verbose", "-v", action="store_true")
parser.add_argument(
    "--cache-dir", "-c",
    help="Where to cache the RSS entry URLS",
    type=DirPath,
    default=Path("cache")
)
parser.add_argument("config", help="A YAML file containing the config to toot RSS feed entries", type=YamlPath)
args = parser.parse_args()

logging.basicConfig(
    level=logging.DEBUG if args.verbose else logging.INFO
)

exit(main(args.config, args.cache_dir))
