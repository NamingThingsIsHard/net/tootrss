import unittest
from pathlib import Path

import feedparser
from feedparser import FeedParserDict

from tootrss.masto import make_toot_text, strip_html

THIS = Path(__file__)
DIR = THIS.parent
RESOURCES = DIR / "resources"


class TestMakeTootText(unittest.TestCase):
    def test_no_summary(self):
        entry = FeedParserDict(
            title="This is a title",
            link="https://something.test/wherever"
        )
        self.assertEqual(
            make_toot_text(entry),
            """This is a title

https://something.test/wherever"""
        )

    def test_short_summary(self):
        entry = FeedParserDict(
            title="This is a title",
            summary="This article has a very short summary",
            link="https://something.test/wherever"
        )
        self.assertEqual(
            make_toot_text(entry),
            """This is a title

This article has a very short summary

https://something.test/wherever"""
        )

    def test_long_summary(self):
        summary = "This article has a very very very long summary" \
                  " that has to be split across multiple lines" \
                  " otherwise the linter wouldn't be very happy with it." \
                  "Sometime later, the summary should be cut off" \
                  " in order to allow for the link to be added." \
                  "Links always take up at most 23 characters in mastodon's eyes," \
                  " even if they can (in reality) surpass that length." \
                  "We just need to add a 100 more characters to make this" \
                  " too long for a single toot and force it to be cut off." \
                  "Random text that will hopefully be cut off please please please"
        link = "https://something.test/wherever"
        entry = FeedParserDict(
            title="This is a title",
            summary=summary
            ,
            link=link
        )
        toot_text = make_toot_text(entry)
        no_link_toot = toot_text[:-(len(link))]

        # Ensure that 23 chars were left for the link
        self.assertEqual(len(no_link_toot), 500 - 23)


class PrepareTootTootText(unittest.TestCase):

    def test_no_html(self):
        self.assertEqual(strip_html("This is a simple text"), "This is a simple text")

    def test_apod_html(self):
        entry = feedparser.parse(str(RESOURCES / "apod.rss")).get('entries')[0]
        self.assertEqual(strip_html(entry.summary), "What's happening at the center of active galaxy 3C 75?")

    def test_quanta_html(self):
        entry = feedparser.parse(str(RESOURCES / "quanta.rss")).get('entries')[0]
        self.assertEqual(
            strip_html(entry.summary),
            "Some animal neuropeptides have been around longer than nervous systems. "
            "The post Brain-Signal Proteins Evolved Before Animals Did first appeared on Quanta Magazine"
        )


if __name__ == '__main__':
    unittest.main()
