Toot RSS feeds to a mastodon account each

# Usage

The easiest way to use this is with docker.
Use the provided [`docker-compose.yml`][dc-prod].

It's easiest to download example folder, adapt the `config.yaml` and run

    docker-compose up    

# How it works

The script connects to a single server, then loops over a list of RSS feeds.
Each RSS feed corresponds to an account on the mastodon server.

Each account has an app (manually created by the account) which contains
 credentials to allow the script to toot with that account.
The RSS feed is then retrieved and compared with a cache to filter out old entries.
The new entries are then tooted and the cache is updated.

[dc-prod]: example/docker-compose.yml
