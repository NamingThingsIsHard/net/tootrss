FROM python:3-slim as base

WORKDIR /app

COPY requirements* ./
RUN pip3 install -r requirements.txt

CMD [ "python3", "-m", "tootrss", "config.yaml"]

FROM base as ci
RUN pip3 install -r requirements.dev.txt

FROM base as prod

COPY tootrss ./tootrss
